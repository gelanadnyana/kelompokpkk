<?php

class Kamera_model {
    // // dbh membuat menampung konksi ke database
    // // stmt untuk memebuat query
        // private $dbh ; //database handler
        // private $stmt ;

        // public function __construct() 
        // {
       // data source name (dsn)
        //     $dsn = 'mysql:host=localhost;dbname=kelompok_pkk';

        //     try{
        //         $this->dbh = new PDO($dsn,'root','');
        //     }catch(PDOExeptioon $e) { //ketika eror tangkap Efxception $e = eror
        //         die( $e->getMassage());
        //     }
        // }

    private $table = "kamera";
    private $db;


    public function __construct() 
    {
        $this->db = new Database;
        $db = $this->db->connect();

    }


    public function getAllKamera()
    {
        // $this->stmt = $this->dbh->prepare('SELECT * FROM kamera');
        // $this->stmt->execute();
        // return $this->stmt->fetchAll(PDO::FETCH_ASSOC);

        $sql = "SELECT * FROM kamera";
        $this->db->query($sql); 
        $stmt = $this->db->resultSet();
        return $stmt;
    }

    public function tambahDataKamera($data)
    {
        // var_dump($data);
        // die;
        $query = " INSERT INTO kamera (merek, gambar, model, speksifikasi, harga)
                VALUES
                (:merek, :gambar, :model, :speksifikasi, :harga)";

                $this->db->query($query);
                // $this->db->bind('gambar', $data['gambar']);
                $this->db->bind('merek', $data['merek']);
                $this->db->bind('gambar', $data['gambar']);
                $this->db->bind('model', $data['model']);
                $this->db->bind('speksifikasi', $data['speksifikasi']);
                $this->db->bind('harga', $data['harga']);

                $this->db->execute();

                return $this->db->rowCount();
                
    }

    public function hapusDataKamera($id)
    {
        $query = "DELETE FROM kamera WHERE id = :id";
        $this->db->query($query);
        $this->db->bind('id', $id);

        $this->db->execute();

        return $this->db->rowCount();
    }

    public function ubahDataKamera($data)
    {
        $harga = $data['harga'];
        $query = "UPDATE kamera SET
            merek = :merek,
            model = :model,
            speksifikasi = :speksifikasi,
            harga = :harga
            WHERE id = :id";

        $this->db->query($query);
        // $this->db->bind('gambar', $data['gambar']);
        $this->db->bind('merek', $data['merek']);
        // $this->db->bind('gambar', $data['gambar']);
        $this->db->bind('model', $data['model']);
        $this->db->bind('speksifikasi', $data['speksifikasi']);
        $this->db->bind('harga', $harga);
        $this->db->bind('id', $data['id']);

        $this->db->execute();

        return $this->db->rowCount();

    }
    public function cariDataKamera()
    {
        $keyword = $_POST['keyword'];
        $query = "SELECT * FROM kamera WHERE merek LIKE :keyword";
        $this->db->query($query);
        $this->db->bind('keyword', "%$keyword%");
        return  $this->db->resultSet();

    }

}



?>