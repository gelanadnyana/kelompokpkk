<div class="container">
        
            <div class="row">
                <div class="col-xl-6 d-flex justify-content-center">
                        <img class="mt-lg-5 shadow" src="<?= BASEURL;?>/image/camera.png" alt="" width="450">
                </div>
                <div class="col-xl-6 p-5" >
                    <!-- form login -->
                    <form class="mt-5 pt-5" method="POST" action="<?= BASEURL;?>/login/auth">
                        <div class="text-center">
                            <h2 class="text-light fw-bold">LOGIN</h2>
                        </div>
                        <div class="mb-3 mt-5">
                          <div class="row">
                            <div class="col-xl-2">
                                <label for="username" class="form-label text-light mt-2">Username</label>
                            </div>
                            <div class="col-xl-10">
                                <input type="text" name="username" class="form-control" id="username" aria-describedby="emailHelp" style="border-radius: 50px;" placeholder="Masukan Username">
                            </div>
                          </div>
                        </div>
                        <div class="mb-3">
                            <div class="row">
                                <div class="col-xl-2">
                                    <label for="exampleInputPassword1" class="form-label text-light mt-2">Password</label>
                                </div>
                                <div class="col-xl-10">
                                    <input type="password" name="password" class="form-control rounded-5" id="exampleInputPassword1" style="border-radius: 50px;" placeholder="Masukan Password">
                                </div>
                            </div>
                        </div>
                       <div class="button d-flex justify-content-center">
                            <button href="index.html" type="submit" class="btn btn-light mt-4" style="color:#BC0707;">Login</button>
                       </div>
                       <a href="<?= BASEURL; ?>/Register/index" class="text-decoration-none text-warning mt-4 d-flex justify-content-center">Register</a>
                      </form>
                      <!-- akhir form login-->
                </div>
                </div>
            </div>
        </div>