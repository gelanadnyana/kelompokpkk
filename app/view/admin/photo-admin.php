<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin Video</title>

    <!-- Custom fonts for this template-->
    <link href="<?= BASEURL; ?>/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?= BASEURL; ?>/css/sb-admin-2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= BASEURL;?>/bootstrap/css/bootstrap.min.css">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">

  
</head>


<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav  sidebar sidebar-dark accordion" id="accordionSidebar" style="background-color: #bc0707;">

            <!-- Sidebar - Brand -->
                <div class="d-flex justify-content-center mt-4">
                    <img class="rounded-circle ms-5" src="<?= BASEURL; ?>/image/camera.png" alt="" width="100">
                </div>
            <!-- Divider -->
            <hr class="">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="<?= BASEURL; ?>/admin">
                  <i class="fas fa-fw fa-tachometer-alt"></i>
                  <span>Dashboard</span></a>
              </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Nav Item - Pages Collapse Menu -->
            <!-- <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                    aria-expanded="true" aria-controls="collapseTwo">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Components</span>
                </a>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Custom Components:</h6>
                        <a class="collapse-item" href="buttons.html">Buttons</a>
                        <a class="collapse-item" href="cards.html">Cards</a>
                    </div>
                </div>
            </li> -->

            <!-- Halaman photography dan videgraphy-->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
                    aria-expanded="true" aria-controls="collapseUtilities">
                    <i class="fas fa-fw fa-wrench"></i>
                    <span>Admin</span>
                </a>
                <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Editor</h6>
                        <a class="collapse-item" href="<?= BASEURL; ?>/admin/photo">Photography</a>
                        <a class="collapse-item" href="<?= BASEURL; ?>/admin/video">Videography</a>
                        <!-- <a class="collapse-item" href="utilities-animation.html">Animations</a>
                        <a class="collapse-item" href="utilities-other.html">Other</a> -->
                    </div>
                </div>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading
            <div class="sidebar-heading">
                Addons
            </div> -->

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages"
                    aria-expanded="true" aria-controls="collapsePages">
                    <i class="fas fa-fw fa-folder"></i>
                    <span>Pages</span>
                </a>
                <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Login Screens:</h6>
                        <a class="collapse-item" href="<?= BASEURL; ?>/admin/logout">Logout</a>
                    
                    </div>
                </div>
            </li>

          
            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

        

        </ul>
        <!-- End of Sidebar -->

        <!-- Halaman dashboard -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                    <!-- Topbar Search -->
                    <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search" action="<?= BASEURL;?>/admin/cari" method="POST">
                        <div class="input-group">
                            <input type="text" class="form-control bg-light border-0 small" placeholder="cari kamera..."
                                aria-label="Search" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-danger" type="button">
                                    <i class="fas fa-search fa-sm"></i>
                                </button>
                            </div>
                        </div>
                    </form>

                   
                </nav>
                <!-- End of Topbar -->

                <!-- Dashboard bawah-->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Photo</h1>

                        <!-- button modals -->
                        <div class="btn btn-primary"  data-bs-toggle="modal" data-bs-target="#exampleModal">
                            Add
                        </div>

                        <!-- isi model -->
                        <!-- modals -->

                        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h1 class="modal-title fs-5" id="exampleModalLabel">Tambah Data Kamera Fotografi</h1>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <form action="<?= BASEURL; ?>/admin/tambah" method="POST">
                                        <div class="gambar">
                                            <label for="gambar" class="form-label">Gambar</label>
                                            <input type="file" name="gambar" id="gambar" class="form-control">
                                        </div>

                                        <div class="merek">
                                            <label for="merek" class=""form-label>Merek</label>
                                            <input type="text" name="merek" id="merek" class="form-control">
                                        </div>
                                        <div class="model">
                                            <label for="model" class="form-label">Model</label>
                                            <input type="text" class="form-control" id="model" name="model">
                                        </div>
                                        <div class="speksifikasi">
                                            <label for="spek" class="form-label">Speksifikasi</label>
                                            <input type="text" id="spek" class="form-control" name="speksifikasi">
                                        </div>
                                        <div class="harga">
                                            <label for="harga" class="form-label">Harga</label>
                                            <input type="text" class="form-control" id="harga" name="harga">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                            <button type="submit" class="btn btn-warning">Tambah Data</button>
                                         </div>
                                    </form>
                                </div>
                                
                                </div>
                            </div>
                            </div>
                    </div>

                    <!-- end modal -->


                    
                    
                    <!-- Content Photography -->
                    <div class="container-fluid">
    
                        <!-- DataTales Example -->
                        <div class="card shadow mb-4">
                            <div class="card-header py-3">
                                <h6 class="m-0 font-weight-bold text-primary">Data</h6>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Foto</th>
                                                <th>Merek</th>
                                                <th>Model</th>
                                                <th>Speksifikasi</th>
                                                <th>Harga</th>
                                                <th>Aksi</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach( $data['kmra'] as $kmr) :?>
                                            <tr>
                                                <td><?= $kmr['id'];?></td>
                                                <td><img src="<?= BASEURL; ?>/image/camera.png" alt="" width="100"></td>
                                                <td><?= $kmr['merek'];?></td>
                                                <td><?= $kmr['model'];?></td>
                                                <td><?= $kmr['speksifikasi'];?></td>
                                                <td><?= $kmr['harga'];?></td>
                                                <td>
                                                <!-- modal edit -->
                                                <div class="btn btn-success"  data-bs-toggle="modal" data-bs-target="#exampleModal<?= $kmr['id'] ?>"> Edit</div>

                                                <div class="modal fade" id="exampleModal<?= $kmr['id'] ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h1 class="modal-title fs-5" id="exampleModalLabel">Edit Data Kamera Fotografi</h1>
                                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form action="<?= BASEURL; ?>/admin/ubah" method="POST">
                                                            <input type="hidden" name="id" value="<?= $kmr['id'];?>">
                                                                <div class="gambar">
                                                                    <label for="gambar" class="form-label">Gambar</label>
                                                                    <input type="file" name="gambar" id="gambar" class="form-control">
                                                                </div>

                                                                <div class="merek">
                                                                    <label for="merek" class=""form-label>Merek</label>
                                                                    <input type="text" name="merek" id="merek" class="form-control" value="<?= $kmr['merek'] ?>">
                                                                </div>
                                                                <div class="model">
                                                                    <label for="model" class="form-label">Model</label>
                                                                    <input type="text" class="form-control" id="model" name="model" value="<?= $kmr['model'] ?>">
                                                                </div>
                                                                <div class="speksifikasi">
                                                                    <label for="spek" class="form-label">Speksifikasi</label>
                                                                    <input type="text" id="spek" class="form-control" name="speksifikasi" value="<?= $kmr['speksifikasi'] ?>">
                                                                </div>
                                                                <div class="harga">
                                                                    <label for="harga" class="form-label">Harga</label>
                                                                    <input type="number" class="form-control" id="harga" name="harga" value="<?= $kmr['harga'] ?>">
                                                                </div>
                                                                <div class="modal-button text-right mt-2">
                                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                                                                    <button type="submit" class="btn btn-success" onclick="return confirm('Yakin di edit?');">Edit Data</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- alhir -->
                                                </td>
                                                <td>
                                                <button class="btn btn-danger delete-button" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini')"><a class="text-light text-decoration-none" href="<?= BASEURL; ?>/admin/hapus/<?=  $kmr['id']?>">Hapus</a></button>    
                                                   

                                                </td>

                                            </tr>
                                            <?php endforeach; ?>

                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
    
                    </div>
                    <!-- Content Row -->


   
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script>
        // function showDeleteConfirmation() {
        //     var modal = document.getElementById("deleteModal");
        //     modal.style.display = "block";
        // }

        // function closeDeleteConfirmation() {
        //     var modal = document.getElementById("deleteModal");
        //     modal.style.display = "none";
        // }

        // function deleteData() {
        //     // Lakukan operasi penghapusan di sini
        //     // Anda dapat menambahkan AJAX request atau menghubungkan ke script PHP untuk menghapus data

        //     // Setelah penghapusan berhasil, tampilkan pesan berhasil
        //     var message = document.getElementById("deleteMessage");
        //     message.innerHTML = "Data berhasil dihapus.";

        //     // Tutup modal konfirmasi
        //     closeDeleteConfirmation();
        // }

        function konfirmasiEdit() {
            var konfirmasi = confirm("Apakah Anda yakin ingin mengedit data ini?");
            if (konfirmasi) {
                // Jika pengguna mengklik "OK", tambahkan logika edit di sini
                alert("Data telah diubah."); // Ini hanya contoh, gantilah dengan logika sesuai kebutuhan Anda
            } else {
                // Jika pengguna mengklik "Batal", tidak melakukan apa-apa
                alert("Edit dibatalkan.");
            }
        }
    </script>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>



    <script src="<?= BASEURL; ?>/vendor/jquery/jquery.min.js"></script>
    <script src="<?= BASEURL; ?>/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script src="<?= BASEURL; ?>/bootstrap/js/bootstrap.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="<?= BASEURL; ?>/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="<?= BASEURL; ?>/js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="<?= BASEURL; ?>/vendor/chart.js/Chart.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="<?= BASEURL; ?>/js/demo/chart-area-demo.js"></script>
    <script src="<?= BASEURL; ?>/js/demo/chart-pie-demo.js"></script>

</body>

</html>