<!-- navbar -->
<nav class="navbar navbar-expand-lg " style="background-color: #bc0707;">
      <div class="container d-flex justify-content-between">
        <div class="img">
          <img src="<?= BASEURL; ?>/image/jodlen.png" alt="" width="80px">
        </div>
        <div class="" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link active text-light" aria-current="page" href="<?= BASEURL;?>/home/index">Beranda</a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link text-light dropdown-toggle" data-bs-toggle="dropdown"  aria-expanded="false">Rekomendadi Kamera</a>
              <ul class="dropdown-menu">
                <li class="dropdown-item"><a href="">Fotografi</a></li>
                <li class="dropdown-item"><a href="">Videografi</a></li>
              </ul>
            </li>
            <li class="nav-item">
              <a class="nav-link text-light" href="<?= BASEURL;?>/kontak/index">Kontak</a>
            </li>
          </ul>
        </div>
        <div class="login">
          <div class="btn btn-light">
          <?php if(isset($_SESSION['login'])) : ?>
            <a href="<?= BASEURL; ?>/login/logout" class="text-decoration-none fw-bold text-danger">Login</a>
            <?php elseif(!isset($_SESSION['login'])) : ?>
              <a href="<?= BASEURL;?>/login" class="text-decoration-none">Logout</a>
              <?php endif;?>
          </div>
        </div>
      </div>
    </nav>
    <!-- end navbar -->

