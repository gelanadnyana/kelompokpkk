<div class="container">
        
            <div class="row">
                <div class="col-xl-6 d-flex justify-content-center">
                        <img class="mt-lg-5 shadow" src="<?= BASEURL;?>/image/camera.png" alt="" width="450">
                </div>
                <div class="col-xl-6 p-5" >
                    <!-- form login -->
                    <form class="mt-5 pt-5" method="POST" action="<?= BASEURL;?>/register/prosesRegister">
                        <div class="text-center">
                            <h2 class="text-light fw-bold">REGISTER</h2>
                        </div>
                        <div class="mb-3 mt-5">
                          <div class="row">
                            <div class="col-xl-2">
                                <label for="username" name="" class="form-label text-light mt-2">Username</label>
                            </div>
                            <div class="col-xl-10">
                                <input type="text" name="username" id="" class="form-control" id="username" style="border-radius: 50px;" >
                            </div>
                          </div>
                        </div>
                        <div class="mb-3">
                            <div class="row">
                                <div class="col-xl-2">
                                    <label for="exampleInputEmail1"  class="form-label text-light mt-2">Email</label>
                                </div>
                                <div class="col-xl-10">
                                    <input type="Email" name="email" class="form-control rounded-5" id="exampleInputEmail1" style="border-radius: 50px;" placeholder="Masukan Email">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <label for="password" class="text-light">Password</label>
                                <input type="password" name="password" class="form-control rounded-5" id="exampleInputPassword1" style="border-radius: 50px;" placeholder="Masukan Password">
                            </div>
                            <div class="col-lg-6">
                                <label for="confirmpassword" class="text-light">Confirm Password</label>
                                <input type="password" name="password2" class="form-control rounded-5" id="exampleInputPassword2" style="border-radius: 50px;" placeholder="Confirm Password">
                            </div>
                        </div>
                       <div class="button d-flex justify-content-center">
                            <button href="index.html" type="submit" class="btn btn-light mt-4" style="color:#BC0707;">Register</button>
                       </div>
                      </form>
                      <!-- akhir form login-->
                </div>
                </div>
            </div>
        </div>