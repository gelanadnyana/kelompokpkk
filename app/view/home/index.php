<!-- section 1-->
<div class="container">
      <div class="row mt-5">
        <div class="col-lg-5 d-flex justify-content-center">
          <img src="<?= BASEURL; ?>/image/foto-depan.jpg" alt="" width="300" height="300" style="object-fit: cover;  border-radius: 30px;">
        </div>
        <div class="col-lg-7">
          <h2 class="text-light fw-bolder">Selamat Datang di Website Jodlen Kamera !!</h2>
          <p class="text-light mt-5 text-break" style="text-align: justify;">Selamat datang di jodlen Kamera, sumber utama rekomendasi kamera yang memenuhi semua kebutuhan fotografi dan videografi Anda! Kami adalah panduan pilihan kamera terpercaya bagi pecinta fotografi dan videografi serta siapa saja yang ingin mengambil gambar dan video berkualitas tinggi.Dengan berbagai merek dan model kamera terbaru serta panduan ahli, kami hadir untuk membantu Anda mengeksplorasi dunia fotografi dan videografi dengan lebih mendalam,sehingga Anda dapat mengabadikan momen-momen berharga dalam hidup Anda dengan lebih baik. Selamat menjelajahi dunia fotografi dan videografi bersama kami di jodlen Kamera!</p>
        </div>
      </div>
    </div>

    <!-- end section 1-->


    <!-- section fotografi -->


    
    <div class="container">
      <h2 class="text-center mt-5 text-light">Rekomendasi Kamera Fotografi</h2>

      <!-- Section 1 -->

      <div class="row mt-5">
        <div class="col-lg-4 mb-5">
          <div class="bg-light w-100  p-4" style="border-radius: 30px; height: 300px">
            <div class="d-flex">
              <div class="mt-4">
                <img src="<?= BASEURL; ?>/image/dslr1.png" alt="" width="150">
              </div>
              <div class="ms-3 mt-4">
                <p style="color: #bc0707;">Merek : Sony</p>
                <p style="color: #bc0707;">Model : Sony A7ll</p>
                <p style="color: #bc0707;">Spek : 24 MP 720p</p>
              </div>
            </div>
            <div class="btn w-50 d-flex mx-auto d-flex justify-content-center align-items-center mt-5" style="background-color: #bc0707;">
              <p class="text-light my-auto">Rp. 10.000.000</p>
            </div>
          </div>
        </div>


        <div class="col-lg-4 mb-5">
          <div class="bg-light w-100  p-4" style="border-radius: 30px; height: 300px">
            <div class="d-flex">
              <div class="mt-4">
                <img src="<?= BASEURL; ?>/image/dslr2.png" alt="" width="150">
              </div>
              <div class="ms-3 mt-4">
                <p style="color: #bc0707;">Merek : Sony</p>
                <p style="color: #bc0707;">Model : Sony A7ll</p>
                <p style="color: #bc0707;">Spek : 24 MP 720p</p>
              </div>
            </div>
            <div class="btn w-50 d-flex mx-auto d-flex justify-content-center align-items-center mt-5" style="background-color: #bc0707;">
              <p class="text-light my-auto">Rp. 10.000.000</p>
            </div>
          </div>
        </div>


        <div class="col-lg-4 mb-5">
        <div class="bg-light w-100  p-4" style="border-radius: 30px; height: 300px">
            <div class="d-flex">
              <div class="mt-4">
                <img src="<?= BASEURL; ?>/image/dslr3.png" alt="" width="150">
              </div>
              <div class="ms-3 mt-4">
                <p style="color: #bc0707;">Merek : Sony</p>
                <p style="color: #bc0707;">Model : Sony A7IV</p>
                <p style="color: #bc0707;">Spek : 22 MP 720p</p>
              </div>
            </div>
            <div class="btn w-50 d-flex mx-auto d-flex justify-content-center align-items-center mt-5" style="background-color: #bc0707;">
              <p class="text-light my-auto">Rp. 10.000.000</p>
            </div>
          </div>
        </div>
      </div>
      <!-- End Section 1 -->



      <!-- section 2 -->

      <div class="row mt-5">
        <div class="col-lg-4">
          <div class="bg-light w-100  p-4" style="border-radius: 30px; height: 300px">
            <div class="d-flex">
              <div class="mt-4">
                <img src="<?= BASEURL; ?>/image/kamera4.png" alt="" width="150">
              </div>
              <div class="ms-3 mt-4">
                <p style="color: #bc0707;">Merek : Sony</p>
                <p style="color: #bc0707;">Model : Sony A7ll</p>
                <p style="color: #bc0707;">Spek : 24 MP 720p</p>
              </div>
            </div>
            <div class="btn w-50 d-flex mx-auto d-flex justify-content-center align-items-center mt-5" style="background-color: #bc0707;">
              <p class="text-light my-auto">Rp. 10.000.000</p>
            </div>
          </div>
        </div>


        <div class="col-lg-4">
          <div class="bg-light w-100  p-4" style="border-radius: 30px; height: 300px">
            <div class="d-flex">
              <div class="mt-4">
                <img src="<?= BASEURL; ?>/image/kamera2.png" alt="" width="150">
              </div>
              <div class="ms-3 mt-4">
                <p style="color: #bc0707;">Merek : Sony</p>
                <p style="color: #bc0707;">Model : Sony A7sll</p>
                <p style="color: #bc0707;">Spek : 34 MP 720p</p>
              </div>
            </div>
            <div class="btn w-50 d-flex mx-auto d-flex justify-content-center align-items-center mt-5" style="background-color: #bc0707;">
              <p class="text-light my-auto">Rp. 17.000.000</p>
            </div>
          </div>
        </div>


        <div class="col-lg-4">
        <div class="bg-light w-100  p-4" style="border-radius: 30px; height: 300px">
            <div class="d-flex">
              <div class="mt-4">
                <img src="<?= BASEURL; ?>/image/kamera6.png" alt="" width="150">
              </div>
              <div class="ms-3 mt-4">
                <p style="color: #bc0707;">Merek : Sony</p>
                <p style="color: #bc0707;">Model : Sony A7IV</p>
                <p style="color: #bc0707;">Spek : 22 MP 720p</p>
              </div>
            </div>
            <div class="btn w-50 d-flex mx-auto d-flex justify-content-center align-items-center mt-5" style="background-color: #bc0707;">
              <p class="text-light my-auto">Rp. 10.000.000</p>
            </div>
          </div>
        </div>
      </div>


      <div class="button d-flex justify-content-center mb-5">
        <div class="btn btn-light mt-5">
          <a href="" class="text-danger text-decoration-none">Selanjutnya >></a>
        </div>
      </div>
    </div>

      <!-- end section 2 -->


      <!-- videografi  -->
      
   


    <!-- section Videografi -->
    <div class="container">
      <h2 class="text-center mt-5 text-light">Rekomendasi Kamera Videografi</h2>



      <!-- section 2 -->
      

      <div class="row mt-5">
      <?php foreach( $data['kmra'] as $kmr) :?>
        <div class="col-lg-4 mb-4">
          <div class="bg-light w-100  p-4" style="border-radius: 30px; height: 300px">
            <div class="d-flex">
              <div class="mt-4">
                <img src="<?= BASEURL; ?>/image/kamera4.png" alt="" width="150">
              </div>
              <div class="ms-3 mt-4">
                <p style="color: #bc0707;">Merek : <?= $kmr['merek'];?></p>
                <p style="color: #bc0707;">Model : <?= $kmr['model'];?></p>
                <p style="color: #bc0707;">Spek : <?= $kmr['speksifikasi'];?></p>
              </div>
            </div>
            <div class="btn w-50 d-flex mx-auto d-flex justify-content-center align-items-center mt-5" style="background-color: #bc0707;">
              <p class="text-light my-auto">Rp<?= $kmr['harga'];?></p>
            </div>
          </div>
          
        </div>
        <?php endforeach;?>
       


       


      <div class="button d-flex justify-content-center mb-5">
        <div class="btn btn-light mt-5">
          <a href="" class="text-danger text-decoration-none">Selanjutnya >></a>
        </div>
      </div>
      






    </div>