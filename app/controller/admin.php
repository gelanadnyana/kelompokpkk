<?php

class admin extends Controller{
    
    public function __construct(){
        if(!isset($_SESSION["login"])){
            header("location: ". BASEURL . "/login");
        }
    }
    public function index(){
        $data['judul'] = 'admin';
        $this->view('template/header', $data);
        $this->view('admin/index');
        $this->view('template/footer');
    }
    public function photo(){
        $data['kmra'] = $this->model('kamera_model')->getAllKamera();
        $this->view('admin/photo-admin', $data);

    }
    public function tambah(){

        if($this->model('kamera_model')->tambahDataKamera($_POST) > 0 ){
            header('Location:'. BASEURL . ('/admin/photo'));
            exit;
        }

    }
    public function video(){
        $this->view('admin/video-admin');
    }
    public function logout(){
        session_unset();
        session_destroy();
        header('location: '. BASEURL . '/login/index');
        exit;
    }

    public function hapus($id)
    {
        if( $this->model('kamera_model')->hapusDataKamera($id) > 0){
            header('location:' . BASEURL . '/admin/photo');
            exit;
        }else{
            header('location:' . BASEURL . '/admin/photo');
            exit;
        }

    }

    public function getUbah()
    {
        $this->model('kamera_model')->getDataUbah($_POST['id']);
    }
    
    public function ubah()
    {
        if($this->model('kamera_model')->ubahDataKamera($_POST) > 0 ){
            echo "
                <script>
                    alert('Data berhasil diedit');
                    window.location.href = '" . BASEURL . "/admin/photo';
                </script>
            ";

        }
   
    }
    public function cari()
    {
        $data['judul'] = 'Cari data';
        $data['kmr'] = $this->model('kameram_model')->cariDataKamera();
        $this->view('template/header', $data);
        $this->view('admin/index');
        $this->view('template/footer');
    }
}
?>