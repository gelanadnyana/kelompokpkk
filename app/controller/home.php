<?php

class home extends Controller{

    public function __construct(){
        if(!isset($_SESSION["login"])){
            header("location: ". BASEURL . "/login");
            exit; // Penting untuk menghentikan eksekusi setelah pengalihan
        }
    }
    public function index()
    {
        $data['judul'] = 'home';
        $kamera_model = $this->model('kamera_model');
        $data['kmra'] = $kamera_model->getAllKamera();
        $data['nama'] = $this->model('User_model')->getUser();
        $this->view('template/header', $data);
        $this->view('template/navbar', $data);
        $this->view('home/index', $data);
        $this->view('template/footer');
    }
   
}