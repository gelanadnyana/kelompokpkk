<?php


class login extends Controller {

    public function __construct(){
        if(isset($_SESSION["login"])){
            header("location: ". BASEURL . "/home");
        }
    }

    public function index(){
        $data['judul'] = 'login';
        $this->view('template/header', $data);
        $this->view('login/index');
        $this->view('template/footer');
    }
    public function auth(){
        $formData = $_POST;
        $userModel = $this->model('User_model');

        $user = $userModel->login($formData);

     
        if ($user) {
            $_SESSION['login'] = true;

        
            if (strtolower($user['role']) === 'admin') {
                header('Location: ' . BASEURL . '/admin');
            } else {
                header('Location: ' . BASEURL . '/user');
            }
        }else{
            echo "
            <script>
            alert('Username Atau Password anda salah');
            window.location.href = 'http://localhost/kelompokpkk/public/login';
            </script>
            ";
        }
    
    
    }
    public function logout(){
        session_unset();
        session_destroy();
        header('location: '. BASEURL . '/login');
        exit;
    }
}