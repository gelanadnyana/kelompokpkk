<?php 

class kontak extends Controller{

    public function __construct(){
        if(!isset($_SESSION["login"])){
            header("location: ". BASEURL . "/login");
            exit; // Penting untuk menghentikan eksekusi setelah pengalihan
        }
    }

    public function index()
    {
        $data['judul'] = 'kontak';
        $data['nama'] = $this->model('User_model')->getUser();
        $this->view('template/header', $data);
        $this->view('template/navbar', $data);
        $this->view('kontak/index');
        $this->view('template/footer');

    }
}




?>